import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/index.vue'
import Member_Login from "../components/login/Member_Login";

Vue.use(VueRouter);

const routes = [
    {
        path: '/home',
        name: 'Home',
        component: Home,
        children: [
            {
                path: '/',
                name: 'welcome',
                component: () => import('@/components/welcome.vue')
            },
            {
                path: '/personalCenter',
                name: 'personalCenter',
                component: () => import('@/components/personalCenter.vue')
            }
        ]
    },
    {
        path: '/about',
        name: 'About',
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: '/',
        name: 'Login',
        component: Member_Login
    },

];

const router = new VueRouter({
    routes
});

export default router
