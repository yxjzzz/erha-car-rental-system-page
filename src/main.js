import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios';
import echarts from 'echarts'

let instance = axios.create({
    timeout: 5000,
    baseURL: 'http://localhost:12188',

});
instance.interceptors.request.use((config) => {

    let strings = config.url.split("?");
    // /sso/userLogin/auth
    if (strings[0] !== '/sso/userLogin/auth' && strings[0] !== '/sso/memberLogin/auth') {
        const token = localStorage.getItem('token')
        if (token) {
            config.headers.myAuthorization = token
        }

    }
    return config
})
Vue.config.productionTip = false;

Vue.prototype.$http = instance;
Vue.prototype.$echarts = echarts
Vue.use(ElementUI);


new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');

function formatDate(date, fmt) {
    date = new Date(date);
    if (typeof(fmt) === "undefined") {
        fmt = "yyyy-MM-dd";
    }
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    let o = {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'H+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds()
    }
    for (let k in o) {
        if (new RegExp(`(${k})`).test(fmt)) {
            let str = o[k] + ''
            fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : ('00' + str).substr(str.length));
        }
    }
    return fmt
}

Vue.filter("FormatDate", function(date, fmt) {
    return formatDate(date, fmt);
});

